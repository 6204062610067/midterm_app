import { Flight } from "../component/flight/data";

export class Mockflight {
  public static mockflight:Flight[]=[{
    fullName:'Pao',
    from:'Bangkok',
    to:'London',
    type:'Return',
    adults:1,
    departure:new Date('06-03-2565'),
    children:0,
    infants:0,
    arrival:new Date('09-03-2565')
  }]

}
